import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws IOException {
        // создание массива случайных чисел и его вывод
        Random rnd = new Random();
        // альтернатива: int [] arr = Arrays.stream(new int[10]).map(x -> rnd.nextInt(10) + 1).toArray();
        int [] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rnd.nextInt(10) + 1;
        }
        System.out.println(Arrays.toString(arr));

        // проверка на строгое возрастание
        boolean isIncreasing = true;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] >= arr[i+1]) {
                isIncreasing = false;
                break;
            }
        }
        if (isIncreasing)
            System.out.println("Массив - строго возрастающая последовательность");
        else
            System.out.println("Массив не является строго возрастающей последовательностью");

        // замена элементов с нечётным индексом на ноль
        for (int i = 1; i < arr.length; i = i + 2) {
            arr[i] = 0;
        }
        System.out.println(Arrays.toString(arr));
    }
}
